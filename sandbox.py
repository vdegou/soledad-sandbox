from leap.soledad.client import Soledad

from random import randint

uuid = 'uuid'
passphrase = u'passphrase'

# Sort of like a seed to encrypt information
secrets_path = './tmp_writer/secrets.json'

shared_db_path = './tmp/db.sqlite'
server_url = 'http://127.0.0.1'

# Filename of the cert
cert_file = './tmp_writer/ca.crt'

# Token which should be -- at some point -- obtained by Bonafide
token = './token'

# TODO: the Soledad constructor triggers a series of methods that will ultimately
# end in a SecretsError exception. The pastebin with the stack trace can be found at:
# https://pastebin.com/FPTHsWGa or in the pastebin dir within this project.
# Trace id: soledad_client_instantiation_error
client_writer = Soledad(
    uuid,
    passphrase,
    secrets_path=secrets_path,
    local_db_path=shared_db_path,
    server_url=server_url,
    cert_file=cert_file,
    auth_token=token
)

# Set up the other client's parameters
# Assuming this client is a device owned by the same user, the uuid, passphrase, secrets_path and local_db_path are
# all the same.
cert_file_reader = './tmp_reader/ca.crt'
token_reader = './tmp_reader/token'

client_reader = Soledad(
    uuid,
    passphrase,
    secrets_path=secrets_path,
    local_db_path=shared_db_path,
    server_url=server_url,
    cert_file=cert_file_reader,
    auth_token=token_reader
)

from twisted.internet import defer, reactor


@defer.inlineCallbacks
def client_usage_example():
    rand_id = 'doc_id_' + str(randint(0, 999))

    # create a document and sync it with the server
    print("Writing a document with id: {}".format(rand_id))
    yield client_writer.create_doc({'my': 'content'}, doc_id=rand_id)

    # reference the document
    doc = yield client_writer.get_doc(rand_id)
    print("Document inserted by client_writer: {}".format(doc))

    # modify the document and sync again
    doc.content = {'new': 'content'}
    yield client_writer.put_doc(doc)

    # print the document
    print("Updated document: {}".format(doc))

    # getting document with different client
    doc_reader = yield client_reader.get_doc(rand_id)
    print("Document obtained by client_reader: {}".format(doc_reader))

    print("Finished transaction.")


d = client_usage_example()
d.addCallback(lambda _: reactor.stop())

reactor.run()
