# The Sandbox
This is a place to test all kinds of bits and pieces of code regarding Soledad and its dependencies in order to get a better understanding of the system.

## Current state of things
### 05/06/18

**Status:** SOLVED

**Goal:** creation and storage of information using the Soledad client object

**Problem:** now that we are able to instantiate a Soledad client object, the point is to use its API in order to securely store information in the local database.

**Solution:** tested the Soledad object API and found the `create_doc()`, `get_doc()`, and `put_doc()` methods. With these, we are able to create and store a document using the Soledad client object, retrieve it for later use, as well as update it. The solution to the above problem was to create one client instance which acts as a single device which writes to the database, and another client instance which acts as a completely different device which reads from the database. We assume both of these devices belong to the same person, so they share the cryptographic material in the `secrets.json` file (two different files found in [tmp_writer](./tmp_writer/) and [tmp_reader](./tmp_reader/) respectively, but with the same content). They also share the local database path, which allows them to use the same database instance for creating and retrieving documents. Finally, the `uuid` and `passphrase` values are also the same, else the client would not be able to decrypt the information used in `secrets.json`.

Changes can be found in commit f6218c5427dbe5e591e2c7c58d293c7601a39857.
---

### 01/06/18

**Status:** SOLVED

**Goal:** offline instantiation of the Soledad client object

**Problem:** the program flow seems to be throwing a SecretsError exception when decrypting the `secrets.json` file.

This is interesting, and very likely a simple mistake on my end. In the `sandbox.py` file, we have a very basic setup for instantiating an offline Soledad client, however, the process fails when decrypting the `secrets.json` file as mentioned previously. The problem stems from the `decrypt()` method found in `leap/soledad/client_secrets/crypto.py`. That's pretty much where the stack trace dies. However, if we follow the code deeper in, we can see that either `_decrypt_v1()` or `_decrypt_v2()` are called depending on the version chosen in the `secrets.json` file. These two methods, in turn, lead to the `_decrypt()` method which later calls the `decrypt_sym()` in `leap/soledad/client/_crypto.py`, and the execution finally dies on line 216 of this file. This information is not relayed on the console, so we might have a case of the SecretsException shadowing information that could be useful in the logs.

On the other hand, if we take a look at the `test.py` file in this repo, we can see that the same information found in the `secrets.json` file is found in the code. And calling the `decrypt()` function that started the error in the previous case seems to work out just fine. No hitches. Lots of confusion.

Hopefully we can get some answers!

**Solution:** had to replace the content of the `secrets.json` file. Changes can be found in commit 8aa3c34670f5a994d793941746dfab35cbf8916a
